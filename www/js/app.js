// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic', 'controllers', 'services'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/login');

  $stateProvider.state('login', {
    url: '/login',
    templateUrl: 'templates/login.html',
    controller: 'LoginCtrl as LoginCtrl' //need as [alias] for ControllerAs syntax
  })
  // qr reader page
  .state('qr', {
    url: '/qr',
    templateUrl: 'templates/qr.html',
    controller: 'QrCtrl'
  })
  // coupon request details page
  .state('crdetails', {
    url: '/crdetails?crid', //crid will appear in $stateParams as a query parameter for passing in and resolving the couponRequest object
    templateUrl: 'templates/crdetails.html',
    controller: 'DetailsCtrl as Dctrl',
    //future TODO make couponRequest call a service that chain resolves all 3 objects b4 entering the new view
    resolve : {// get the appropiate coupon request according to the cr objectId specified by url parameter
      // couponRequest : function($http, $stateParams, $localstorage, $ionicPopup, $q, $state, $location) {
      //   var sessionToken = $localstorage.get('sessionToken');
      //   var config = {
      //     headers : {
      //       "X-Parse-Application-Id": "PHMSnxrbPHQLgOBuE6XbXcjFosZ5QW791H1SLP7p",
      //       "X-Parse-REST-API-Key": "uClAe1OJ7miqLjxTtGROUwqymJ94arQM6Ly55s3u",
      //       'X-Parse-Session-Token' : sessionToken
      //     }
      //   };
      //   var url = 'https://api.parse.com/1/classes/CouponRequest/' + $stateParams.crid;
      //   console.log("before deferred");
      //   var deferred = $q.defer();
      //   console.log("url: " + url);

      //   $http.get(url, config) // If you use .then here, you must also return a promise, and that promise will be chained to the original promise.
      //   // see https://github.com/angular-ui/ui-router/wiki
        
      //   .then(function(data) {
      //     console.log('in then');
      //     console.log('data: ' + JSON.stringify(data));
      //     deferred.resolve(data);
      //   },
      //   function(error) {
      //     console.log('error: ' + JSON.stringify(error));
      //     deferred.reject(error);

      //     (function() {
      //       var alertPopup = $ionicPopup.alert({
      //         title: 'Error',
      //         template: 'Invalid Coupon Request Id'
      //       });
      //       alertPopup.then(function(res) {
      //         //$state.go('qr');
      //         $state.go($state.current, {}, {reload: true});
      //       });
      //     })();

      //     // // wrapping in a timeout so the dialog doesn't freeze the app
      //     // setTimeout(function() {
      //     //     alert("Error: Invalid Coupon Request Id");    
      //     // }, 0);
      //   });
      //   return deferred.promise;
      // }

      results : function(Redeem, $stateParams) {
        return Redeem.redeem($stateParams.crid);
      }
    }
  });
});
