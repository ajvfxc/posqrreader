angular.module('controllers', [])
.controller('QrCtrl', ['$scope', '$ionicPlatform', '$state' ,function($scope, $ionicPlatform, $state) {

  $scope.$on('$ionicView.enter', function() {
    console.log('After $ionicView.enter');
    //make sure ionic is loaded properly first
    $ionicPlatform.ready(function() {
      console.log("In ready");
      cordova.plugins.barcodeScanner.scan(

        // success callback function
        function (result) {
          console.log("result: " + JSON.stringify(result));
            // wrapping in a timeout so the dialog doesn't free the app
            // setTimeout(function() {
            //     alert("We got a barcode\n" +
            //           "Result: " + result.text + "\n" +
            //           "Format: " + result.format + "\n" +
            //           "Cancelled: " + result.cancelled);
            // }, 0);
            //once we have the qr text, use it to get the appropiate CouponRequest obj, then User and Coupon objs
            $state.go('crdetails', {crid : result.text});
        },

        // error callback function
        function (error) {
            alert("Scanning failed: " + error);
        },

        // options object
        {
            "preferFrontCamera" : true,
            "showFlipCameraButton" : false
        }
      );
    });
  });
}])

.controller('DetailsCtrl', ['results', '$coupon', '$user', '$http', '$ionicPopup', '$state', function(results, $coupon, $user, $http, $ionicPopup, $state) {
  console.log('DetailsCtrl');
  //console.log("coupon request: " + vm.cr);
  //console.log("DetailsCtrl arguments: " + JSON.stringify(arguments));
  var vm = this;
  //var headers = couponRequest.config.headers;
  // var couponId = couponRequest.data.couponId;
  // var userId = couponRequest.data.userId;
  // var crId = couponRequest.data.objectId;

  //vm.headers = JSON.stringify(headers);
  vm.results = results;
  console.log("results: " + JSON.stringify(results));
  //vm.coupon = {};
  vm.couponName = results.coupon.title;
  vm.couponId = results.coupon.objectId;
  vm.couponExpire = results.coupon.expireDatetime.iso;
  vm.couponDesc = results.coupon.description;
  vm.username = results.cashier.username;
  vm.expired = false;
  vm.redeemed = results.couponRequest.redeemed;
  vm.success = results.result.success;

  // //redeem button. send put request to Parse to update coupRequest.redeemed
  // vm.redeem = function() {
  //   var url = 'https://api.parse.com/1/classes/CouponRequest/' + crId;
  //   var data = {"redeemed" : true};
  //   var config = {};
  //   config['headers'] = headers;
  //   $http.put(url, data, config).then(function(response) {
  //     console.log("cr update success, response: " + JSON.stringify(response));
  //     vm.redeemed = true;
  //     //alert success
  //     (function() {
  //         var alertPopup = $ionicPopup.alert({
  //           title: 'Success',
  //           template: 'The coupon has been successfully redeemed.'
  //         });
  //         alertPopup.then(function(res) {
  //           //$state.go('qr');
  //         });
  //       })();
  //   }, function(error) {
  //     console.log('cr update error: ' + JSON.stringify(error));
  //     (function() {
  //         var alertPopup = $ionicPopup.alert({
  //           title: 'Error',
  //           template: 'There was an error in redeeming the coupon. Please try again.'
  //         });
  //         alertPopup.then(function(res) { });
  //     })();
  //   });
  // };

  // // get user object
  // $user.get(userId, headers).then(function(user) {
  //   console.log("get user success");
  //   vm.username = user.data.username;
  // },
  // function(error) {
  //   console.log("get user error: " + JSON.stringify(error));
  // });

  // //get coupon object and redeem
  // $coupon.get(couponId, headers).then(function(coupon) {
  //   var now, expireDate;
  //   console.log("get coupon success");
  //   vm.coupon = JSON.stringify(coupon);
  //   vm.couponName = coupon.data.title;
  //   vm.couponId = coupon.data.objectId;
  //   vm.couponDesc = coupon.data.description;
  //   vm.couponExpire = coupon.data.expireDatetime.iso;

  //   //check if the coupon's brand matches the login's brand
    
  //   //check if expired
  //   now = new Date();
  //   expireDate = new Date(coupon.data.expireDatetime["iso"]);
  //   if(now >= expireDate) {
  //     vm.expired = true;
  //     //pop up alert
  //     (function() {
  //       var alertPopup = $ionicPopup.alert({
  //         title: 'Error',
  //         template: 'This coupon has already expired'
  //       });
  //       alertPopup.then(function(res) { });
  //     })();
  //   }
  //   else {//NOT EXPIRED
  //     //check if cr is already redeemed. //check here so that coupon info displays regardless
  //     if(couponRequest.data.redeemed) {
  //       //pop up alert
  //       (function() {
  //         var alertPopup = $ionicPopup.alert({
  //           title: 'Error',
  //           template: 'This coupon has already been redeemed'
  //         });
  //         alertPopup.then(function(res) { });
  //       })();
  //     }
  //     else {// NOT ALREADY REDEEMED
  //       //call redeem
  //       vm.redeem();
  //     }
  //   }
  // },
  // function(error) {
  //   console.log("get coupon error: " + JSON.stringify(error));
  // });
  
}])

.controller('LoginCtrl', ['$login', '$localstorage', '$state','$stateParams', '$scope',
function($login, $localstorage, $state, $stateParams, $scope) {

  var vm = this;
  vm.user = {'email' : 'cashier1@yahoo.com', 'password' : '123'};

  vm.logMeIn = function() { //the function login is reserved in ionic? Doesn't work.
    console.log("logMeIn()");
    console.log("username: ", vm.user.email);
    console.log("password: ", vm.user.password);
     vm.loginFailure = false;
     var results = {'success': false, 'errormsg' : ''};
     var redirectUrl = $stateParams.redirect;

    //needs to be a $scope variable so ng-model can access it in the template
    $login.loginParse(vm.user.email, vm.user.password).success(function(data) {
      console.log('Login success. Parse returned: ', data);
      results['success'] = true;
      console.log("success(): results obj declared in logMeIn():", results);

      //put sessionToken and other info in localstorage
      $localstorage.set('sessionToken', data.sessionToken);
      $localstorage.set('objectId', data.objectId);
      $localstorage.set('username', vm.user.email);

      // //go to home page after login
      // console.log('$stateParams: ', $stateParams);
      // if(redirectUrl !== undefined)
      //   $location.url($stateParams.redirect);
      // else
      //   $location.url('/');

      //go to qr page with specified params
      $state.go('qr');

    }).error(function(data){
      vm.loginFailure = true;
      vm.errormsg = data.error;
      console.log('login error! ', data.error);
    });
  };
}]);