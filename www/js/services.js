angular.module('services', [])

//service for accessing localstorage
.factory('$localstorage', ['$window', function($window) {
  return {
    set: function(key, value) {
      $window.localStorage[key] = value;
    },
    get: function(key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    },
    setObject: function(key, value) {
      $window.localStorage[key] = JSON.stringify(value);
    },
    getObject: function(key) {
      return JSON.parse($window.localStorage[key] || '{}');
    },
    clear: function() {
      $window.localStorage.clear();
    },
    removeItem: function(key) {
      $window.localStorage.removeItem(key);
    }
  };
}])

//service for checking if user is logged in
.factory('$login', ['$localstorage', '$http', '$ionicPopup', '$location', function($localstorage, $http, $ionicPopup, $location) {
  return {
    isLoggedin: function() {
      if($localstorage.get('sessionToken') === undefined)
        return false;
      else
        return true;
    },
    //log in to Parse using its REST API and return a promise for the caller to handle
    loginParse: function(username, password) { //the function login is reserved in ionic? Doesn't work.
      var loginUrl = 'https://api.parse.com/1/login';
      var config = {
        params: {
          'username' : username,
          'password' : password
        },
        headers: {
          "X-Parse-Application-Id": "PHMSnxrbPHQLgOBuE6XbXcjFosZ5QW791H1SLP7p",
          "X-Parse-REST-API-Key": "uClAe1OJ7miqLjxTtGROUwqymJ94arQM6Ly55s3u"
        }
      };
      return $http.get(loginUrl, config);
    },
    loginVerify: function(cancelUrl) { //if logged in, returns true, else returns false and pops up login dialog box
      //show dialog box with login page link
      if(!this.isLoggedin()) {
        var loginPopup = $ionicPopup.confirm({
          title: 'Login',
          template: 'Please log in to use this feature.'
        });
        loginPopup.then(function(response) {
          if(response) {
            var prevUrl = $location.url();
            console.log(prevUrl);
            $location.url('/login?redirect=' + prevUrl); //specify redirect url after login
          }
          else { //USER PRESSED CANCEL
            if(cancelUrl !== undefined)
              $location.url(cancelUrl);
          }
        },
        function(response) {
          console.log("loginpopup error");
        });
        return false;
      }
      else {
        return true;
      }
    }
  };
}])

//service for getting a specific coupon
.factory('$coupon', ['$http', '$localstorage', function($http, $localstorage) {
  return {
    get : function(couponId, headers) {
      var url = 'https://api.parse.com/1/classes/Coupon/';
      var config = {};
      config['headers'] = headers;

      return $http.get(url + couponId, config);
    }
  };
}])

//service for getting a specific user
.factory('$user', ['$http', function($http) {
  return {
    get : function (userId, headers) {
      var url = 'https://api.parse.com/1/classes/_User/';
      var config  = {};
      config['headers'] =  headers;

      return $http.get(url + userId, config);
    }
  };
}])

//service to call custom redeem function on Parse
.factory('Redeem', ['$http', '$localstorage', '$q', '$ionicPopup', '$state', function($http, $localstorage, $q, $ionicPopup, $state) {
  return {
    redeem : function(crId) {
      var sessionToken = $localstorage.get('sessionToken');
      var url = 'https://api.parse.com/1/functions/redeem';
      var config = {
          headers : {
            "X-Parse-Application-Id": "PHMSnxrbPHQLgOBuE6XbXcjFosZ5QW791H1SLP7p",
            "X-Parse-REST-API-Key": "uClAe1OJ7miqLjxTtGROUwqymJ94arQM6Ly55s3u",
            'X-Parse-Session-Token' : sessionToken
          }
        };
      var bodyData = {"crId" : crId};
      // returns a promise to resolve in ui-router
      return $http.post(url, bodyData, config).then(function(data) {
        console.log("data: " + JSON.stringify(data));
        if(data.data.result.result.success) {
          return data.data.result;
        }
        else {
          (function() {
            var alertPopup = $ionicPopup.alert({
              title: 'Error',
              template: data.data.result.result.errormsg
            });
            alertPopup.then(function(res) {
              //$state.go('qr');
              //$state.go('qr', {}, {reload: true});
            });
          })();
          //return $q.reject(data.data.result.result);
          return data.data.result;
        }
      },
      function(error) {
        console.log("error after http post to redeem: " + JSON.stringify(error));
        return $q.reject(error);

      });
    }
  };
}])

// service for getting the coupons that we've already requested. (pressed rubcoupon)
.factory('$couponReq', ['$http', '$localstorage', '$stateParams', function($http, $localstorage, $stateParams) {
  //var savedNeedsUpdate = true; //bool var to check if the info in the saved tab needs to be updated e.g. when a new coupon request gets created
  return {
    get : function() {
      var userId = $localstorage.get('objectId');
      var sessionToken = $localstorage.get('sessionToken');
      var config = {
        'headers' : {
          "X-Parse-Application-Id": "PHMSnxrbPHQLgOBuE6XbXcjFosZ5QW791H1SLP7p",
          "X-Parse-REST-API-Key": "uClAe1OJ7miqLjxTtGROUwqymJ94arQM6Ly55s3u",
          'X-Parse-Session-Token' : sessionToken
        },
        'params' : {
          'where' : {
            'userId' : userId,
            'couponId' : $stateParams.chatId
            //'redeemed' : {"$ne" : false}
          }
        }
      };
      return $http.get('https://api.parse.com/1/classes/CouponRequest', config);
    }
  };
}]);
